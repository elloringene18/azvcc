<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    protected $fillable = ['session_id','title','description','slug','start_at','end_at','user_id'];

    public $dates = ['start_at','end_at'];

    public function kol(){
        return $this->hasOne('App\Models\User','id','user_id');
    }

    public function attendees(){
        return $this->belongsToMany('App\Models\User','session_attendees');
    }
}
