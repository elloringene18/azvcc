<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SessionAttendee extends Model
{
    protected $fillable = ['session_id','user_id'];

    public $timestamps = false;
}
