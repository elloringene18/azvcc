<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password','username'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function scopeOfRole($query, $role_id){

        return $query->whereHas('role', function($q) use($role_id){
            $q->where('role_id',$role_id);
        });

    }
    /**
     * Every user has one Role
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role(){
        return $this->belongsToMany('App\Models\Role','role_users');
    }

    /**
     * Get User Full name
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getFullNameAttribute(){
        return $this->first_name . ' ' . $this->last_name;
    }
}
