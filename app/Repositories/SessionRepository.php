<?php

namespace App\Repositories;

use App\Models\Session;
use App\Repositories\Eloquent\CanCreateSlug;
use Guzzle\Http\Message\Request;
use OpenTok\OpenTok;
use App\Models\User;
use App\Models\Session as Conferences;

use App\Services\OpentokService;
use App\Services\UserService;

class SessionRepository {

    use CanCreateSlug;

    public function __construct(
        Conferences $session,
        OpentokService $opentokService,
        UserService $userService
    ){
        $this->model = $session;
        $this->opentok = $opentokService->init();
        $this->userService = $userService;
    }

    public function store($request){

        $attendees = $request->input('attendees');

        $data = $request->except('_token','date','attendees');
        $data['slug'] = $this->generateSlug($data['title']);
        $data['start_at'] = strtotime($data['start_at']);
        $data['end_at'] = strtotime($data['end_at']);

        $session = $this->opentok->createSession();

        $data['session_id'] = $session->getSessionId();

        if (empty($data['session_id'])) {
            throw new \Exception("An open tok session could not be created");
        }

        $conference = Conferences::create($data);

        if(!$conference)
            throw new \Exception("Session could not be create. Please try again");
        elseif($attendees)
            $conference->attendees()->sync(array_values($attendees));

        return $conference;
    }

    public function update($request){

        $attendees = $request->input('attendees');
        $session = Conferences::where('id',$request->input('session_id'))->first();

        $data = $request->except('_token','date','attendees','session_id');
        $data['slug'] = $this->generateSlug($data['title']);
        $data['start_at'] = strtotime($data['start_at']);
        $data['end_at'] = strtotime($data['end_at']);

        $session->update($data);

        if(!$session)
            throw new \Exception("Session could not be updated. Please try again");
        elseif($attendees)
            $session->attendees()->sync(array_values($attendees));

        return redirect()->back();
    }
}