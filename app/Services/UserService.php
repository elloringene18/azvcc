<?php

namespace App\Services;

use OpenTok\OpenTok;
use App\Models\User;

class UserService {

    public function getUserByRole($role_id){
        return User::whereHas('role', function($q) use($role_id){
            $q->where('role_id',$role_id);
        })->get();
    }

    public function getUsersByRoleArray($role_id){
        $users = User::ofRole($role_id)->get()->toArray();

        foreach($users as $index=>$user)
            $users[$index]['full_name'] = $user['first_name'] . ' ' . $user['last_name'];

        return $users;
    }

    public function getUserByRoleList($role_id){
        $data = User::ofRole($role_id)->select('id','first_name','last_name')->get();

        $users = [];
        foreach($data as $datum){
            $users[$datum->id] = $datum->first_name . ' ' . $datum->last_name;
        }

        return $users;
    }
}