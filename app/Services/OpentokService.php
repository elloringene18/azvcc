<?php

namespace App\Services;

use OpenTok\OpenTok;

class OpentokService {

    public function init(){
        $apiKey = env('OPENTOK_KEY');
        $apiSecret = env('OPENTOK_SECRET');
        return new OpenTok($apiKey,$apiSecret);
    }
}