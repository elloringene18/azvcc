<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/chat', 'MainController@index');
Route::get('get-convert', 'MainController@getConvert');
Route::post('convert', 'MainController@convert');


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});

Route::group(['middleware' => 'web'], function () {
    Route::auth();
    Route::get('/home', 'HomeController@index');
});


/**
 * Conference Routes
 */


Route::get('conferences', ['as' => 'kol.conferences', 'uses' => 'ConferenceController@index'] );
Route::get('conferences/{room_id}', ['as' => 'kol.conferences.show', 'uses' => 'ConferenceController@show']);

/**
 * Admin Routes
 */

Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function() {

    Route::get('conferences', ['as' => 'admin.conferences', 'uses' => 'ConferenceController@index']);
    Route::get('conferences/create', ['as' => 'admin.conferences.create', 'uses' => 'ConferenceController@create']);
    Route::post('conferences/store', ['as' => 'admin.conferences.store', 'uses' => 'ConferenceController@store']);
    Route::get('conferences/{room_id}/edit', ['as' => 'admin.conferences.edit', 'uses' => 'ConferenceController@edit']);
    Route::post('conferences/update', ['as' => 'admin.conferences.update', 'uses' => 'ConferenceController@update']);

    Route::get('users', ['as' => 'admin.users', 'uses' => 'UserController@index']);
    Route::get('users/create', ['as' => 'admin.users.create', 'uses' => 'UserController@create']);
    Route::post('users/store', ['as' => 'admin.users.store', 'uses' => 'UserController@store']);
    Route::get('users/{user_id}/edit', ['as' => 'admin.users.edit', 'uses' => 'UserController@edit']);
    Route::post('users/update', ['as' => 'admin.users.update', 'uses' => 'UserController@update']);

});


/**
 * API Routes
 */

Route::group(['prefix' => 'api'], function() {

    Route::post('get-users-by-role', ['as' => 'api.getUsersByRole', 'uses' => 'APIController@getUsersByRole']);

});