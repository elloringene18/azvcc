<?php

namespace App\Http\Controllers;

use App\Services\UserService;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class APIController extends Controller
{
    public function __construct(UserService $userService){
        $this->userService = $userService;
    }

    public function getUsersByRole(){
        return json_encode($this->userService->getUsersByRoleArray($_POST['role_id']));
    }
}
