<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use OpenTok\OpenTok;
use OpenTok\MediaMode;
use OpenTok\ArchiveMode;

class ChatController extends Controller
{
    public function __construct(){
        $this->opentok = new OpenTok('45471362','104ef2e285c126c03b9014f25b729818a440dc39');
    }

    public function index(){


        try {

            $session = $this->opentok->createSession();
            $token = OpentokApi::generateToken($sessionId,
                array(
                    'role' => Role::PUBLISHER
                )
            );
        } catch(OpenTokException $e) {
            // do something here for failure
        }

        // Store this sessionId in the database for later use
        $sessionId = $session->getSessionId();

        echo $sessionId;
    }
}
