<?php

namespace App\Http\Controllers\Admin;

use App\Models\SessionAttendee;
use App\Models\User;
use App\Services\OpentokService;
use App\Services\UserService;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Repositories\Eloquent\CanCreateSlug;
use App\Repositories\Eloquent\CanFindBySlug;
use OpenTok\OpenTok;
use Tomcorbett\OpentokLaravel\Facades\OpentokApi;

use App\Models\Session as Conferences;

use App\Repositories\SessionRepository;

class ConferenceController extends Controller
{
    use CanFindBySlug, CanCreateSlug;

    public function __construct(
            SessionRepository $sessionRepository,
            Conferences $session,
            OpentokService $opentokService,
            UserService $userService
    ){
        $this->model = $session;
        $this->opentok = $opentokService->init();
        $this->userService = $userService;
        $this->sessionRepository = $sessionRepository;
        $this->middleware('web');
        $this->middleware('auth');

        parent::__construct();
    }

    public function index(){
        $sessions = Conferences::with('kol')->get();
        return view('admin.conferences.index', compact('sessions'));
    }

    public function create(){
        $kols = $this->userService->getUserByRoleList(2);

        return view('admin.conferences.create',compact('kols'));
    }

    public function store(Request $request){
        $this->sessionRepository->store($request);
        return redirect(route('admin.conferences'));
    }

    public function update(Request $request){
        $this->sessionRepository->update($request);
        return redirect(route('admin.conferences'));
    }

    public function edit($session_slug){
        $conference = Conferences::with('kol')->where('slug',$session_slug)->first();
        $attendees = SessionAttendee::where('session_id', $conference->id)->lists('user_id');

        $kols = $this->userService->getUserByRoleList(2);

        return view('admin.conferences.create',compact('kols','conference','attendees'));
    }

}
