<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Services\UserService;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function __construct(
        UserService $userService
    ){
        $this->userService = $userService;
        $this->middleware('web');
        $this->middleware('auth');

        parent::__construct();
    }

    public function index(){
        $users = User::with('role')->get();
        return view('admin.users.index', compact('users'));
    }

    public function create(){
        $kols = $this->userService->getUserByRoleList(2);

        return view('admin.users.create',compact('kols'));
    }

    public function store(Request $request){
        $this->sessionRepository->store($request);
        return redirect(route('admin.users'));
    }

    public function update(Request $request){
        $this->sessionRepository->update($request);
        return redirect(route('admin.users'));
    }

    public function edit($session_slug){
        $conference = Conferences::with('kol')->where('slug',$session_slug)->first();
        $attendees = SessionAttendee::where('session_id', $conference->id)->lists('user_id');

        $kols = $this->userService->getUserByRoleList(2);

        return view('admin.users.create',compact('kols','conference','attendees'));
    }

}
