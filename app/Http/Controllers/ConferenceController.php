<?php

namespace App\Http\Controllers;

use App\Services\OpentokService;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use OpenTok\OpenTok;
use App\Models\Session as Conference;

class ConferenceController extends Controller
{
    public function __construct(OpentokService $opentokService){
        $this->opentok = $opentokService->init();
        $this->middleware('web');
        $this->middleware('auth');

        parent::__construct();
    }

    public function index(){
        return view('guest.conferences.index');
    }

    public function show($slug){
        $apiKey = env('OPENTOK_KEY');

        $session = Conference::with('kol')->where('slug',$slug)->first();
        $sessionId = $session->session_id;
        $token = $this->opentok->generateToken($sessionId);

        // Create an archive using custom options
//        $archiveOptions = array(
//            'name' => 'Important Presentation',     // default: null
//            'hasAudio' => true,                     // default: true
//            'hasVideo' => true,                     // default: true
//            'outputMode' => OutputMode::COMPOSED  // default: OutputMode::COMPOSED
//        );
//
//        $archive = $this->opentok->startArchive($sessionId);

//        return view( 'guest.conferences.room', compact('token','sessionId','apiKey','session'));
        return view( (Auth::user()->role[0]->id == 2 ?  'kol' : 'guest') . '.conferences.room', compact('token','sessionId','apiKey','session'));
    }

    public function disconnectAll($sessionId){
        $apiKey = '45471362';
        $apiSecret = '104ef2e285c126c03b9014f25b729818a440dc39';

        $this->opentok = new OpenTok($apiKey,$apiSecret);

        // new session
//        $session    = $this->opentok->createSession();
        $sessionId  = '2_MX40NTQ3MTM2Mn5-MTQ1NDMxMDM3MjkwNH5lclB2Nkt0ZEV5SEdkbGpKamNwK1B4b1d-UH4';

        // check if it's been created or not (could have failed)
        if (empty($sessionId)) {
            throw new \Exception("An open tok session could not be created");
        }

        $token = $this->opentok->generateToken($sessionId);

        // Create an archive using custom options
        $archiveOptions = array(
            'name' => 'Important Presentation',     // default: null
            'hasAudio' => true,                     // default: true
            'hasVideo' => true,                     // default: true
            'outputMode' => OutputMode::COMPOSED  // default: OutputMode::COMPOSED
        );

        $archive = $this->opentok->startArchive($sessionId);

        dd($archive);
        return view('guest.conferences.room', compact('token','sessionId','apiKey'));
    }
}
