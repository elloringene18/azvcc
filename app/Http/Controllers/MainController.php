<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use RobbieP\CloudConvertLaravel\Facades\CloudConvert;

class MainController extends Controller
{
    public function __construct(){
    }

    public function index(){
        echo 'HOME';
    }

    public function getConvert(){
        return view('converter.index');
    }

    public function convert(Request $request){
        # Convert a PowerPoint presentation to a set of images, let's say you only want slides 2 to 4
        # This will save presentation-2.jpg, presentation-3.jpg and presentation-4.jpg
        CloudConvert::file($request->file('file'))->pageRange(1, 4)->to('jpg');
    }
}
