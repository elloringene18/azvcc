// Initialize an OpenTok Session object
var session = TB.initSession(sessionId);

// Initialize a Publisher, and place it into the element with id="publisher"
// var publisher = TB.initPublisher(apiKey, 'user');
var connectedUsers = [];
// Attach event handlers
session.on({
    connectionCreated: function (event) {
        console.log('Session Connected');
        // if (event.connection.connectionId != session.connection.connectionId) {
        //    connectedUsers.push(event.connection.id);
        // }
        connectedUsers.push(event.connections[0]);
    },
    // connectionDestroyed: function (event) {
    //   console.log('Session Destroyed');
    //   console.log(event);
    // },
    // This function runs when session.connect() asynchronously completes
    sessionConnected: function(event) {
        // Publish the publisher we initialzed earlier (this will trigger 'streamCreated' on other
        var publisherProperties = {width: 400, height:300 , name : name  };
        publisher = OT.initPublisher('user', publisherProperties);
        session.publish(publisher);
    },
    sessionDisconnected: function(event) {
        // Publish the publisher we initialzed earlier (this will trigger 'streamCreated' on other
        // clients)
        console.log('User has disconnected');
    },

    // This function runs when another client publishes a stream (eg. session.publish())
    streamCreated: function(event) {
        // Create a container for a new Subscriber, assign it an id using the streamId, put it inside
        // the element with id="subscribers"

        console.log(event);
        var subItem = document.createElement('div');
        subItem.id = event.stream.streamId;
        subItem.insertAdjacentHTML('beforeend', (event.stream.name));
        subItem.className += "subscriber ";

        var str = null;
        var streamProperties = {};
        var targetDiv = document.getElementById('publisher')

        //if(event.stream.name == 1)
        console.log(event.stream.name + " == " + kolName);
        if(event.stream.name == kolName )
            targetDiv = document.getElementById('speaker');
        else
            targetDiv = document.getElementById('subscribers');

        if( document.getElementsByClassName('OT_subscriber').length < 1 ){

            var subContainer = document.createElement('div');
            subContainer.id = 'stream-' + event.stream.streamId;
            subContainer.className += " active";
            subItem.className += " active";

            if( event.stream.name == name && !document.getElementById('user')){
                targetDiv = document.getElementById('user');
                streamProperties = {
                    insertMode: 'append'
                };
            }
            else if( event.stream.name == kolName && !document.getElementById('speaker')) {
                var publisherDiv = document.createElement('div');
                publisherDiv.id = 'speaker';
                document.getElementById('speaker-panel').appendChild(publisherDiv);
                streamProperties = {
                    style : {
                        nameDisplayMode : 'off'
                    },
                    insertMode: 'append'
                };
                targetDiv = publisherDiv;
            } else {
                streamProperties = { insertMode: 'append' };
            }
            // Subscribe to the stream that caused this event, put it inside the container we just made
            console.log(streamProperties);
            str = session.subscribe(event.stream, targetDiv , streamProperties);

            if(event.stream.name != kolName){
                hightlightSubscriber(event.stream);
                attachHighlightEvent(str,event.stream);
            }
            checkHighlight();
        } else {

            str = session.subscribe(event.stream, targetDiv , { insertMode: 'append' });

            attachHighlightEvent(str,event.stream);
        }

        console.log('STREAM CREATED');
    },

    // This function runs when another client publishes a stream (eg. session.publish())
    streamDestroyed: function(event) {
        document.getElementById('stream-' + event.stream.streamId).remove();

        console.log('Stream Destroyed');
        console.log(session.connections);
    }

});

session.on("signal:chat", function(event) {
    console.log(event);
    var date = new Date('H:m');

    dom = '<li class="chat-msg"><div class="author pull-left">'+event.data.username+'</div>'+
          '<div class="time pull-right">'+timeNow()+'</div>'+
          '<div class="pull-left width-full msg">'+event.data.text+'</div></li>';

    $('#chat-messages').prepend(dom);
});

session.on("signal:slideChange", function(event) {
    var owl = $("#slider").data('owlCarousel');
    console.log(event);
    console.log('Slide Changed to ' + event.data.slideIndex);
    $("#slider").trigger('to.owl.carousel', [event.data.slideIndex - 1]);
});

session.on("signal:disconnectAll", function(event) {
    session.disconnect();
});

console.log(
    moment(sessionStart).format('MM-D-YYYY h:mm') + ' > '+
    moment(currentTime).format('MM-D-YYYY h:mm') + ' > '+
    moment(sessionEnd).format('MM-D-YYYY h:mm')
);
if(
    moment().format('MM-D-YYYY, h:mm') >= moment(sessionStart).format('MM-D-YYYY, h:mm') &&
    moment().format('MM-D-YYYY, h:mm') <= moment(sessionEnd).format('MM-D-YYYY, h:mm')
){
    connectSession();
} else {
    console.log('NOPE');
}


//get query string param
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}


$(document).ready(function(){

    var msg = "";

    $('#submit-msg').on('click',function(){
        sendMsg();
    });


    $('#connect').on('click',function(){
        connectSession();
    });

    $('#box-msg').keypress(function(e) {
        if(e.which == 13)
            sendMsg();
    });

    /**
     * LIVE SLIDER SOCKET CODE
     * @type {*|jQuery|HTMLElement}
     */

    var slider = $("#slider");
    var fetching = false;

    slider.owlCarousel({
        callbacks:true,
        loop: true,
        draggable: false,
        items: 1,
        mouseDrag : false,
        touchDrag : false,
        thumbs: false
    });

    $('.brown').css('height',$('.slide-panel').height()+20+'px');

});

$(window).load(function () {
    $('#connectionsPanel').css('height',$('.brown').height()-20+'px');
    connectSession();
})
function sendMsg(){
    msg = $('#box-msg').val();
    $('#box-msg').val('');

    session.signal(
        {
            type: 'chat',
            data:  { text : msg , username : $('#username').val() }
        },
        function(error) {
            if (error) {
                console.log("signal error ("
                + error.code
                + "): " + error.message);
            } else {
                console.log("signal sent.");
                $('#box-msg').val('')
            }
        }
    );
}

function connectSession() {
// Connect to the Session using the 'apiKey' of the application and a 'token' for permission
    session.connect(apiKey, token, function(error) {
        if (error) {
            console.log(error.message);
        }
    });
}

function timeNow() {
    var d = new Date(),
        h = (d.getHours()<10?'0':'') + d.getHours(),
        m = (d.getMinutes()<10?'0':'') + d.getMinutes();

    return h + ':' + m;
}

function attachHighlightEvent(dom, stream){

    $(dom.element).on('click',function(){
        $(this).parent().children().removeClass('active');
        $(this).addClass('active');
        // Subscribe to the stream that caused this event, put it inside the container we just made

        hightlightSubscriber(stream);
        console.log('Switch User has subscribed');
    });
}

function hightlightSubscriber(stream){

    if(!document.getElementById('highlighted-subscriber')){
        var publisherDiv = document.createElement('div');
        publisherDiv.id = 'highlighted-subscriber';
        publisherDiv.className += 'publisher';
        document.getElementById('highlighted-subscriber-panel').appendChild(publisherDiv);

        session.subscribe(stream, document.getElementById('highlighted-subscriber'));
    } else
        session.subscribe(stream, document.getElementById('highlighted-subscriber'));
}

function checkHighlight(){
    if($('#subscribers .OT_subscriber.active').length==0 && $('#subscribers .OT_subscriber').length > 0)
        $('#subscribers .OT_subscriber').first().addClass('active');
}