// Initialize an OpenTok Session object
var session = TB.initSession(sessionId);

// Initialize a Publisher, and place it into the element with id="publisher"
// var publisher = TB.initPublisher(apiKey, 'user');
var connectedUsers = [];
// Attach event handlers
session.on({
    connectionCreated: function (event) {
        console.log('Session Connected');
        // if (event.connection.connectionId != session.connection.connectionId) {
        //    connectedUsers.push(event.connection.id);
        // }
        console.log(event);
        connectedUsers.push(event.connections[0]);
    },
    // connectionDestroyed: function (event) {
    //   console.log('Session Destroyed');
    //   console.log(event);
    // },
    // This function runs when session.connect() asynchronously completes
    sessionConnected: function(event) {
        // Publish the publisher we initialzed earlier (this will trigger 'streamCreated' on other
        var publisherProperties = {width: 400, height:300 , name : name };
        publisher = OT.initPublisher('user', publisherProperties);
        session.publish(publisher);
    },
    sessionDisconnected: function(event) {
        // Publish the publisher we initialzed earlier (this will trigger 'streamCreated' on other
        // clients)
        console.log('User has disconnected');
    },

    // This function runs when another client publishes a stream (eg. session.publish())
    streamCreated: function(event) {
        // Create a container for a new Subscriber, assign it an id using the streamId, put it inside
        // the element with id="subscribers"

        console.log(event);
        var subItem = document.createElement('div');
        subItem.id = event.stream.streamId;
        subItem.insertAdjacentHTML('beforeend', (event.stream.name));
        subItem.className += "subscriber ";

        var str = null;

        if(document.getElementsByClassName('OT_subscriber').length < 1){

            var subContainer = document.createElement('div');
            subContainer.id = 'stream-' + event.stream.streamId;
            subContainer.className += " active";
            subItem.className += " active";

            if(!document.getElementById('publisher')){
                var publisherDiv = document.createElement('div');
                publisherDiv.id = 'publisher';
                document.getElementById('publisherPanel').appendChild(publisherDiv);
            }
            // Subscribe to the stream that caused this event, put it inside the container we just made
            str = session.subscribe(event.stream, document.getElementById('publisher'));
            console.log('First User has subscribed');
        }

        str = session.subscribe(event.stream, document.getElementById('subscribers') , {
            insertMode: 'append',
            style : {
                //nameDisplayMode : 'on'
            }
        });


        $(str.element).on('click',function(){
            console.log('Switch');
            $(this).parent().children().removeClass('active');
            $(this).addClass('active');
            // Subscribe to the stream that caused this event, put it inside the container we just made

            if(!document.getElementById('publisher')){
                var publisherDiv = document.createElement('div');
                publisherDiv.id = 'publisher';
                publisherDiv.className += 'publisher';
                document.getElementById('publisherPanel').appendChild(publisherDiv);
                session.subscribe(event.stream, document.getElementById('publisher'));
            } else
                session.subscribe(event.stream, document.getElementById('publisher'));

            console.log('Switch User has subscribed');
        });

    },

    // This function runs when another client publishes a stream (eg. session.publish())
    streamDestroyed: function(event) {
        document.getElementById('stream-' + event.stream.streamId).remove();

        console.log('Stream Destroyed');
        console.log(session.connections);
    }

});

session.on("signal:chat", function(event) {
    console.log(event);
    $('#chat-messages').prepend('<li><b>'+event.data.username+'</b>: '+event.data.text+'</li>');
});

session.on("signal:slideChange", function(event) {
    var owl = $("#slider").data('owlCarousel');
    console.log(event);
    console.log('Slide Changed to ' + event.data.slideIndex);
    $("#slider").trigger('to.owl.carousel', [event.data.slideIndex - 1]);
});

session.on("signal:disconnectAll", function(event) {
    session.disconnect();
});

// Connect to the Session using the 'apiKey' of the application and a 'token' for permission
session.connect(apiKey, token, function(error) {
    if (error) {
        console.log(error.message);
    }
});

//get query string param
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}


$(document).ready(function(){

    var msg = "";

    $('#submit-msg').on('click',function(){
        msg = $('#box-msg').val();
        $('#box-msg').val('');

        session.signal(
            {
                type: 'chat',
                data:  { text : msg , username : $('#username').val() }
            },
            function(error) {
                if (error) {
                    console.log("signal error ("
                    + error.code
                    + "): " + error.message);
                } else {
                    console.log("signal sent.");
                    $('#box-msg').val('')
                }
            }
        );

    });

    $('#dcAll').on('click',function(){
        session.signal(
            {
                type: 'disconnectAll'
            },
            function(error) {
                if (error) {
                    console.log("signal error ("
                    + error.code
                    + "): " + error.message);
                } else {
                    console.log("signal sent.");
                }
            }
        );
    });

    $('#sthumbs img').on('click',function(){
        console.log('Slide Index' + $(this).eq());
        $("#slider").trigger('to.owl.carousel', [$(this).eq()]);

    });

    /**
     * LIVE SLIDER SOCKET CODE
     * @type {*|jQuery|HTMLElement}
     */

    var slider = $("#slider");
    var sthumbs = $("#sthumbs");
    var fetching = false;

    slider.owlCarousel({
        callbacks:true,
        loop: true,
        items: 1,
        thumbs: false
    });


    sthumbs.owlCarousel({
        callbacks:true,
        items: 4,
        thumbs: false
    });

    // Listen to owl events:
    slider.on('changed.owl.carousel', function(event) {
        console.log(event);
        if(fetching==false){
            fetching = true;
            session.signal(
                {
                    type: 'slideChange',
                    data:  { slideIndex : event.item.index - 1 }
                },
                function(error) {
                    if (error) {
                        console.log("signal error ("
                        + error.code
                        + "): " + error.message);
                    } else {
                        console.log("signal sent.");
                        $('#box-msg').val('')
                    }
                    fetching = false;
                }
            );
        }
    })

});

