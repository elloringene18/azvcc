<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

use App\Models\User;
use App\Models\Role;
use App\Models\RoleUser;

class UserSeeder extends Seeder
{

    public function __construct(User $user){
        $this->user = $user;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker\Factory::create();

        //disable foreign key check for this connection before running seeders
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $this->user->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $users = [
            ['username' => 'admin' ,'first_name' => 'Gene' ,'last_name' => 'Ellorin' , 'email' => 'admin@test.com' , 'password' => Hash::make('secret')],
            ['username' => 'kol' ,'first_name' => 'Nico','last_name' => 'Ellorin' , 'email' => 'kol@test.com' , 'password' => Hash::make('secret')],
            ['username' => 'guest' ,'first_name' => 'Katrine ','last_name' => 'Martinez' , 'email' => 'guest@test.com' , 'password' => Hash::make('secret')],
        ];

        $role = [
                    [ 'role_id' => 1 ],
                    [ 'role_id' => 2 ],
                    [ 'role_id' => 3 ],
                ];

        foreach($users as $index=>$user){
            $user = User::create($user);
            $user->role()->sync($role[$index]);
        }

        $limit = 20;

        for ($i = 0; $i < $limit; $i++) {
            $fname = $faker->firstName;
            $lname = $faker->lastName;
            $username = strtolower($fname.'.'.$lname);

            $user = User::create([
                'username' => $username,
                'first_name' => $fname,
                'last_name' => $lname,
                'email' => $faker->email,
                'password' => Hash::make('secret')
            ]);

            $user->role()->sync(['role_id' => rand(1, 3)]);
        }

    }
}
