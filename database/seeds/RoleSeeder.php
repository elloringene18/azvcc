<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

use App\Models\Role;

class RoleSeeder extends Seeder
{

    public function __construct(Role $role){
        $this->roles = $role;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //disable foreign key check for this connection before running seeders
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $this->roles->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $roles = [
            ['name' => 'Administrator' , 'slug' => 'administrator'],
            ['name' => 'KOL' , 'slug' => 'kol'],
            ['name' => 'Guest' , 'slug' => 'guest'],
        ];

        foreach($roles as $role)
            Role::create($role);
    }
}
