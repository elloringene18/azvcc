@extends('layouts.app')

@section('styles')
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Conferences</div>

                <div class="panel-body">
                    <table class="table">
                        <th>Title</th>
                        <th>KOL</th>
                        <th>Start</th>
                        <th>End</th>
                        <th>Action</th>
                        <tbody>
                            @foreach($sessions as $session)
                                <tr>
                                    <td>{{ $session->title }}</td>
                                    <td>{{ $session->kol->fullName }}</td>
                                    <td>{{ $session->start_at->format('m-d-Y h:m') }}</td>
                                    <td>{{ $session->end_at->format('m-d-Y h:m') }}</td>
                                    <td>
                                        <a target="_blank" href="{{ route('kol.conferences.show',$session->slug) }}">View</a> /
                                        <a href="{{ route('admin.conferences.edit',$session->slug) }}">Edit</a> /
                                        <a href="#">Delete</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
@endsection