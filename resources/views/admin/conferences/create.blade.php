@extends('layouts.app')

@section('styles')
    <link href='{{ asset('public/daterangepicker/daterangepicker.css') }}' rel='stylesheet' />
    <link href='{{ asset('public/magicsuggest/magicsuggest-min.css') }}' rel='stylesheet' />
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Create a new Session</div>
                <div class="panel-body">
                    {!! Form::open(['route'=> isset($conference) ? 'admin.conferences.update' : 'admin.conferences.store']) !!}

                        @if(isset($conference))
                            {!! Form::hidden('session_id', $conference->id) !!}
                        @endif

                        Title : {!! Form::input('text','title', isset($conference) ? $conference->title : null ,['required' ,'placeholder'=>'Title of Session','class' => 'form-control']) !!}<br>

                        Date : {!! Form::input('text','date',null ,['required' ,'id' => 'daterange' ,'placeholder'=>'Title of Presentation','class' => 'form-control']) !!}<br>
                        {!! Form::hidden('start_at', isset($conference) ? $conference->start_at : null , ['id'=>'start_at']) !!}
                        {!! Form::hidden('end_at', isset($conference) ? $conference->end_at : null , ['id'=>'end_at']) !!}

                        Description : {!! Form::textarea('description', isset($conference) ? $conference->description : null ,['required' ,'placeholder'=>'Description','class' => 'form-control']) !!}<br>
                        KOL : {!! Form::select('user_id', $kols , null ,['required' ,'class' => 'form-control']) !!}<br>

                        Attendees:
                        <div id="ms-complex-templating"></div><br>
                        {!! Form::submit(isset($conference) ? 'Update' : 'Create') !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script src='{{ asset('public/fullcalendar/lib/moment.min.js') }}'></script>
    <script src='{{ asset('public/fullcalendar/lib/jquery.min.js') }}'></script>
    <script src='{{ asset('public/daterangepicker/daterangepicker.js') }}'></script>
    <script src='{{ asset('public/magicsuggest/magicsuggest-min.js') }}'></script>

    <script type="text/javascript">
    $(function() {
        $('#daterange').daterangepicker({
            timePicker: true,
            timePickerIncrement: 30,
            locale: {
                format: 'MM/DD/YYYY h:mm A'
            }
        });

        $('#daterange').on('apply.daterangepicker', function(ev, picker) {
            updateDates()
        });

        var picker = $('#daterange').data('daterangepicker');

        function updateDates(){
            $('#start_at').val(picker.startDate);
            $('#end_at').val(picker.endDate);
        }

        $(document).ready(function(t) {
            updateDates();

            var attendees = "{{ isset($attendees) ? $attendees : '[{}]' }} ";
            var users = '[{}]';

            console.log(attendees);

            $.ajax({
              url  : "{{ route('api.getUsersByRole') }}",
              method  : "POST",
              data : { role_id : 3 },
              success : function(data) {
                users = JSON.parse(data);
                // note that it would be a lot more proper to use CSS classes here instead of inline style
              }
            }).done(function(){
                $('#ms-complex-templating').magicSuggest({
                    data: users,
                    value  : JSON.parse(attendees).length > 1 ? JSON.parse(attendees) : null ,
                    renderer: function(data){
                        return '<div style="padding: 5px; overflow:hidden;">' +
    //                            '<div style="float: left;"><img src="' + data.picture + '" /></div>' +
                            '<div style="float: left; margin-left: 5px">' +
                                '<div style="font-weight: bold; color: #333; font-size: 10px; line-height: 11px">' + data.first_name + ' ' +data.last_name + '</div>' +
                                '<div style="color: #999; font-size: 9px">' + data.email + '</div>' +
                            '</div>' +
                        '</div><div style="clear:both;"></div>'; // make sure we have closed our dom stuff
                    },
                    allowFreeEntries : false,
                    selectFirst      : true,
                    displayField     : 'full_name',
                    name             : 'attendees[]'
                });

            });

        });
    });
    </script>
@endsection