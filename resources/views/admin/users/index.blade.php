@extends('layouts.app')

@section('styles')
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Conferences</div>

                <div class="panel-body">
                    <table class="table">
                        <th>Name</th>
                        <th>Role</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Action</th>
                        <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>{{ $user->fullName }}</td>
                                    <td>{{ isset($user->role[0]) ? ($user->role[0]->name ? $user->role[0]->name : false ) : 'N/A' }}</td>
                                    <td>{{ $user->username }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>
                                        <a target="_blank" href="{{ route('kol.conferences.show',$user->username) }}">View</a> /
                                        <a href="{{ route('admin.conferences.edit',$user->username) }}">Edit</a> /
                                        <a href="#">Delete</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
@endsection