@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('public/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/owl.theme.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/owl.transitions.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/owl.carousel.css') }}">
@endsection

@section('content')

<!-- Trigger the modal with a button -->
{{--<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>--}}
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

    <div class="row room">
        <div class="col-md-12">

          <div class="col-md-2 group brown no-padding">
            <div class="padding-t-60 padding-lr-15">
              <h3 class="panel-title">SPEAKER
                  <a href="#" class="info-bt">
                    <img src="{{ asset('public/img/info-icon.png') }}">
                  </a>
              </h3>

              <div id="speaker-panel">
                <div id="speaker"></div>
              </div>
              <p id="speaker-name">{{ $session->kol->fullName }}</p>
            </div>
              <hr>

            <div class="padding-t-60 padding-lr-15">
              <div class="panel panel-default chat">
                <div class="panel-heading">
                  <h3 class="panel-title pink"><img class="icon" src="{{ asset('public/img/chat-icon.png') }}"> CHAT</h3>
                </div>
                <div class="panel-body">
                  <div class="chat-box" data-mcs-theme="minimal-dark">
                      <ul id="chat-messages">
                      {{--<li class="chat-msg"><div class="author pull-left">Gene</div>--}}
                                {{--<div class="time pull-right">10:30</div>--}}
                                {{--<div class="pull-left width-full msg">Um faceste modigenem--}}
                                                                      {{--iusam quam arum quiate--}}
                                                                      {{--nis alit, quassitisto et</div></li>--}}
                      {{--<li class="chat-msg"><div class="author pull-left">Gene</div>--}}
                                {{--<div class="time pull-right">10:30</div>--}}
                                {{--<div class="pull-left width-full msg">Sample Tessxt </div></li>--}}
                      {{--<li class="chat-msg"><div class="author pull-left">Gene</div>--}}
                                {{--<div class="time pull-right">10:30</div>--}}
                                {{--<div class="pull-left width-full msg">Sample Tessxt </div></li>--}}
                      {{--<li class="chat-msg"><div class="author pull-left">Gene</div>--}}
                                {{--<div class="time pull-right">10:30</div>--}}
                                {{--<div class="pull-left width-full msg">Sample Tessxt </div></li>--}}

                      </ul>
                  </div>
                  <textarea id="box-msg" class="width-full" rows="3" placeholder="Message..."></textarea><br>
                  <button id="submit-msg" style="display: none;">Send Message</button>
                </div>
              </div>
            </div>
              {{--<button id="connect">Connect</button>--}}
<!--         <div class="row">
              <a target="_blank" href="http://leadingbrands.me">
                <img width="100%" style="margin-bottom:20px" src="<?php echo $_SERVER['REQUEST_SCHEME']  ?>://<?php echo $_SERVER['HTTP_HOST'] . '/kolchat'  ?>/img/gifs/Conferences.gif">
              </a>
            </div>  -->
          </div>

          <div class="col-md-8 group padding-t-20 slide-panel">

              <h3 id="countdown" class="text-center"></h3>
              {{--<h3 id="countdown2" class="text-center"></h3>--}}
              <div id="slider" data-slider-id="1" class="owl-carousel owl-theme">
                <div class="item"><img height="250" src="{{ asset('public/img/slides/slide1.jpg') }}" alt="The Last of us"></div>
                <div class="item"><img height="250" src="{{ asset('public/img/slides/slide2.jpg') }}" alt="GTA V"></div>
                <div class="item"><img height="250" src="{{ asset('public/img/slides/slide3.jpg') }}" alt="Mirror Edge"></div>
                <div class="item"><img height="250" src="{{ asset('public/img/slides/slide4.jpg') }}" alt="The Last of us"></div>
                <div class="item"><img height="250" src="{{ asset('public/img/slides/slide5.jpg') }}" alt="GTA V"></div>
                <div class="item"><img height="250" src="{{ asset('public/img/slides/slide6.jpg') }}" alt="Mirror Edge"></div>
                <div class="item"><img height="250" src="{{ asset('public/img/slides/slide7.jpg') }}" alt="The Last of us"></div>
                <div class="item"><img height="250" src="{{ asset('public/img/slides/slide8.jpg') }}" alt="GTA V"></div>
                <div class="item"><img height="250" src="{{ asset('public/img/slides/slide9.jpg') }}" alt="Mirror Edge"></div>
              </div>

              <div id="sthumbs" data-slider-id="2" class="owl-carousel owl-theme">
                <div class="item"><img width="50" src="{{ asset('public/img/slides/slide1.jpg') }}" alt="The Last of us"></div>
                <div class="item"><img width="50" src="{{ asset('public/img/slides/slide2.jpg') }}" alt="GTA V"></div>
                <div class="item"><img width="50" src="{{ asset('public/img/slides/slide3.jpg') }}" alt="Mirror Edge"></div>
                <div class="item"><img width="50" src="{{ asset('public/img/slides/slide4.jpg') }}" alt="The Last of us"></div>
                <div class="item"><img width="50" src="{{ asset('public/img/slides/slide5.jpg') }}" alt="GTA V"></div>
                <div class="item"><img width="50" src="{{ asset('public/img/slides/slide6.jpg') }}" alt="Mirror Edge"></div>
                <div class="item"><img width="50" src="{{ asset('public/img/slides/slide7.jpg') }}" alt="The Last of us"></div>
                <div class="item"><img width="50" src="{{ asset('public/img/slides/slide8.jpg') }}" alt="GTA V"></div>
                <div class="item"><img width="50" src="{{ asset('public/img/slides/slide9.jpg') }}" alt="Mirror Edge"></div>
              </div>

              <div class="description">
                  <div class="col-md-9 description margin-tb-10">
                  <div class="row " style="padding-right: 25px">
                    <h4>SESSION DESCRIPTION</h4>
                    {{--<p>{{ $session->description }}</p>--}}
                    <p>Um faceste modigenem iusam quam arum quiate nis alit, quassitisto etLit la coressit et fugia dolorio nsequi ne poreces tiaspel
                       iciaspe reperrum ipid maximet lant quae. Nam, sit harchitatur, nos et dolor aut et volupiet hilluptat quaecus doluptatem as est,
                       ni aut officatiur, quo beaquas invelenit quodiat uribus sunt.
                       Udit apidelestrum aut essunt, unt moluptur, incto consequ iandenihilis ea dolor solorem vides prorpor emporep udantiam fugit,
                       cum quasped magni simostiur, erspidios ea audis delendio volectum quiaerum, omni consed ut et entiusciis voluptas expella
                       borent.</p>
                   </div>
                  </div>
                  <div class="col-md-3 margin-t-10">
                      <div class="row">
                          <div style="padding-left: 15px" id="publisherPanel" >
                            <div id="user" class="user"></div>
                            <input type="hidden" id="username" value="{{ Auth::user()->fullName }}">
                          </div>
                      </div>
                  </div>
              </div>
          </div>

          <div class="col-md-2 group margin-t-20">
              <div class="row">
                <div id="connectionsPanel">
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h3 class="panel-title pink">GUESTS ONLINE</h3>
                      </div>
                      <div class="panel-body" style="background-color: #fff;padding-bottom: 0" id="highlighted-subscriber-panel">
                        <div id="highlighted-subscriber"></div>
                      </div>

                      <div class="panel-body" style="background-color: #fff;" id="subscriber-panel">
                        <div id="subscribers">
                        </div>
                        {{--<button id="dcAll">Disconnect All</button>--}}
                      </div>
                    </div>
                </div>
              </div>
          </div>

    </div>
@endsection

@section('js')

    <script type="text/javascript">
        var apiKey = '<?php echo $apiKey; ?>';
        var sessionId = '<?php echo $sessionId; ?>';
        var token = '<?php echo $token; ?>';
        var name = '<?php echo Auth::user()->fullName; ?>';
        var kolName = '<?php echo $session->kol->fullName; ?>';
        var role = '<?php echo Auth::user()->role[0]->id; ?>';
        var sessionStart = '<?php echo $session->start_at; ?>';
        var sessionEnd = '<?php echo $session->end_at; ?>';
        var currentTime = '<?php echo date('m-d-Y h:m') ?>';
    </script>

    <script src="//static.opentok.com/webrtc/v2.2/js/TB.min.js"></script>
    <script src="{{ asset('public/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('public/js/moment.js') }}"></script>
    <script src="{{ asset('public/opentok/guest.js') }}"></script>
    <script src="{{ asset('public/js/OwlCarousel2Thumbs.min.js') }}"></script>
    <script src="{{ asset('public/jqueryCountdown/jquery.countdown.min.js') }}"></script>

    <script type="text/javascript">
    console.log('Current Date : ' + currentTime);
    console.log('Session Start : ' + sessionStart);
    console.log('Session End : ' + sessionEnd);
      $('#countdown').countdown('2016-02-08 12:22:10', function(event) {
//        $(this).html(event.strftime('Session starts in %-D Day(s) %H:%M:%S'));
        $(this).html('<span class="countdown">60</span><br><span class="count-label">MINUTES LEFT</span>');
      }).on('finish.countdown', function(event) {
        if (event.elapsed) { // Either true or false
          console.log('Session Started');
          $('#countdown2').countdown(sessionEnd, function(event) {
//                  $(this).html(event.strftime('%-H:%-M:%S'));
                  $(this).html(event.strftime('%-H:%-M:%S'));
                });
        } else {
        }
      });

//        $('#countdown2').countdown(sessionEnd, function(event) {
//            $(this).html(event.strftime('%M:%S'));
//          });
    </script>
@endsection