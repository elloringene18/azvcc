@extends('layouts.app')

@section('styles')
    <link href='{{ asset('public/fullcalendar/fullcalendar.css') }}' rel='stylesheet' />
    <link href='{{ asset('public/fullcalendar/fullcalendar.print.css') }}' rel='stylesheet' media='print' />
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Conferences</div>
                <div class="panel-body">
	                <table>
	                    <th>
	                        <td>ID</td>
	                        <td>Title</td>
	                    </th>
	                    <tbody>
	                        <tr>
	                            <td>sadasd</td>
	                            <td>sadasd</td>
	                        </tr>
	                    </tbody>
	                </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

	$(document).ready(function() {

		$('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			defaultDate: '2016-01-12',
			editable: true,
			eventLimit: true, // allow "more" link when too many events
			events: [
				{
					title: 'All Day Event',
					start: '2016-01-01'
				},
				{
					title: 'Long Event',
					start: '2016-01-07',
					end: '2016-01-10'
				},
				{
					id: 999,
					title: 'Repeating Event',
					start: '2016-01-09T16:00:00'
				},
				{
					id: 999,
					title: 'Repeating Event',
					start: '2016-01-16T16:00:00'
				},
				{
					title: 'Conference',
					start: '2016-01-11',
					end: '2016-01-13'
				},
				{
					title: 'Meeting',
					start: '2016-01-12T10:30:00',
					end: '2016-01-12T12:30:00'
				},
				{
					title: 'Lunch',
					start: '2016-01-12T12:00:00'
				},
				{
					title: 'Meeting',
					start: '2016-01-12T14:30:00'
				},
				{
					title: 'Happy Hour',
					start: '2016-01-12T17:30:00'
				},
				{
					title: 'Dinner',
					start: '2016-01-12T20:00:00'
				},
				{
					title: 'Birthday Party',
					start: '2016-01-13T07:00:00'
				},
				{
					title: 'Click for Google',
					url: 'http://google.com/',
					start: '2016-01-28'
				}
			]
		});

	});

</script>
<style>

	body {
		margin: 40px 10px;
		padding: 0;
		font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
		font-size: 14px;
	}

	#calendar {
		max-width: 900px;
		margin: 0 auto;
	}

</style>

@endsection

@section('js')
    <script src='{{ asset('public/fullcalendar/lib/moment.min.js') }}'></script>
    <script src='{{ asset('public/fullcalendar/lib/jquery.min.js') }}'></script>
    <script src='{{ asset('public/fullcalendar/fullcalendar.min.js') }}'></script>
@endsection