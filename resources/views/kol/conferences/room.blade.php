@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('public/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/owl.theme.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/owl.transitions.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/owl.carousel.css') }}">
@endsection

@section('content')
<div class="container" style="width:1240px">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Room</div>

                <div class="panel-body">

                          <div class="col-md-2 group">
                            <div class="row">
                              <div class="panel panel-default">
                                <div class="panel-heading">
                                  <h3 class="panel-title">Speaker</h3>
                                  <div id="speaker"></div>
                                  {{ $session->kol->fullName }}
                                </div>

                              </div>

                              <div class="panel panel-default">
                                <div class="panel-heading">
                                  <h3 class="panel-title">Chat</h3>
                                </div>
                                <div class="panel-body">
                                  <textarea id="box-msg" class="width-100" rows="3"></textarea><br>
                                  <button id="submit-msg">Send Message</button>

                                  <ul id="chat-messages">

                                  </ul>
                                </div>
                              </div>

                            </div>
                    <!--         <div class="row">
                              <a target="_blank" href="http://leadingbrands.me">
                                <img width="100%" style="margin-bottom:20px" src="<?php echo $_SERVER['REQUEST_SCHEME']  ?>://<?php echo $_SERVER['HTTP_HOST'] . '/kolchat'  ?>/img/gifs/Conferences.gif">
                              </a>
                            </div>  -->
                          </div>


                          <div class="col-md-8 group">

                              <div id="slider" data-slider-id="1" class="owl-carousel owl-theme">
                                <div class="item"><img height="250" src="{{ asset('public/img/slides/slide1.jpg') }}" alt="The Last of us"></div>
                                <div class="item"><img height="250" src="{{ asset('public/img/slides/slide2.jpg') }}" alt="GTA V"></div>
                                <div class="item"><img height="250" src="{{ asset('public/img/slides/slide3.jpg') }}" alt="Mirror Edge"></div>
                                <div class="item"><img height="250" src="{{ asset('public/img/slides/slide4.jpg') }}" alt="The Last of us"></div>
                                <div class="item"><img height="250" src="{{ asset('public/img/slides/slide5.jpg') }}" alt="GTA V"></div>
                                <div class="item"><img height="250" src="{{ asset('public/img/slides/slide6.jpg') }}" alt="Mirror Edge"></div>
                                <div class="item"><img height="250" src="{{ asset('public/img/slides/slide7.jpg') }}" alt="The Last of us"></div>
                                <div class="item"><img height="250" src="{{ asset('public/img/slides/slide8.jpg') }}" alt="GTA V"></div>
                                <div class="item"><img height="250" src="{{ asset('public/img/slides/slide9.jpg') }}" alt="Mirror Edge"></div>
                              </div>

                              <div id="sthumbs" data-slider-id="2" class="owl-carousel owl-theme">
                                <div class="item"><img width="50" src="{{ asset('public/img/slides/slide1.jpg') }}" alt="The Last of us"></div>
                                <div class="item"><img width="50" src="{{ asset('public/img/slides/slide2.jpg') }}" alt="GTA V"></div>
                                <div class="item"><img width="50" src="{{ asset('public/img/slides/slide3.jpg') }}" alt="Mirror Edge"></div>
                                <div class="item"><img width="50" src="{{ asset('public/img/slides/slide4.jpg') }}" alt="The Last of us"></div>
                                <div class="item"><img width="50" src="{{ asset('public/img/slides/slide5.jpg') }}" alt="GTA V"></div>
                                <div class="item"><img width="50" src="{{ asset('public/img/slides/slide6.jpg') }}" alt="Mirror Edge"></div>
                                <div class="item"><img width="50" src="{{ asset('public/img/slides/slide7.jpg') }}" alt="The Last of us"></div>
                                <div class="item"><img width="50" src="{{ asset('public/img/slides/slide8.jpg') }}" alt="GTA V"></div>
                                <div class="item"><img width="50" src="{{ asset('public/img/slides/slide9.jpg') }}" alt="Mirror Edge"></div>
                              </div>

                              <div class="col-md-8">
                                Session Description<br>
                                <p>The second best practice comes hand in hand with the first one: Don't relay on certain conversion time. We do put a lot of effort to provide fast and reliable conversions. Most our conversions are done within seconds. Nevertheless, there might be the case that your usually fast conversions takes longer: If files are transferred through networks congestions comes into play, which we sometimes cannot influence. Also, even in the times of cloud computing, it might happen that we have to queue your request until our infrastructure has been scaled. Thus your application design should not relay on a certain time. If your conversions are user-initiated you should always let your users know what exactly is going on and provide them progress information, if possible.</p>
                              </div>
                              <div class="col-md-4">
                                  <div class="panel-body" id="publisherPanel">
                                    <div id="publisher" class="publisher"></div>
                                    <div id="user" class="user"></div>
                                    <input type="hidden" id="username" value="{{ Auth::user()->fullName }}">
                                  </div>
                              </div>
                          </div>

                          <div class="col-md-2 group">
                              <div class="row">
                                <div id="connectionsPanel">
                                    <div class="panel panel-default">
                                      <div class="panel-heading">
                                        <h3 class="panel-title">Users online</h3>
                                      </div>
                                      <div class="panel-body">
                                        <div id="subscribers">
                                        </div>
                                        <button id="dcAll">Disconnect All</button>
                                      </div>
                                    </div>
                                </div>
                              </div>
                          </div>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')

    <script type="text/javascript">
        var apiKey = '<?php echo $apiKey; ?>';
        var sessionId = '<?php echo $sessionId; ?>';
        var token = '<?php echo $token; ?>';
        var name = '<?php echo Auth::user()->fullName; ?>';
    </script>

    <script src="//static.opentok.com/webrtc/v2.2/js/TB.min.js"></script>

    <script src="{{ asset('public/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('public/js/OwlCarousel2Thumbs.min.js') }}"></script>
    <script src="{{ asset('public/opentok/room.js') }}"></script>


@endsection