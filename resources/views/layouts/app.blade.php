<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ asset('public/customscroll/jquery.mCustomScrollbar.min.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/main.css') }}" rel="stylesheet">

    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>

    @yield('styles')

    <link href="{{ asset('public/css/overrides.css') }}" rel="stylesheet">
</head>
<body id="app-layout">
    <nav class="navbar navbar-default">
        <div class="container" style="width: 90%;">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img id="az-logo" src="{{ asset('public/img/az-logo-purple.png') }}" width="100%">
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                {{--<ul class="nav navbar-nav">--}}
                    {{--<li><a href="{{ url('/home') }}">Home</a></li>--}}
                    {{--<li class="dropdown">--}}
                        {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">--}}
                            {{--Conferences <span class="caret"></span>--}}
                        {{--</a>--}}
                        {{--<ul class="dropdown-menu" role="menu">--}}
                            {{--<li><a href="{{ route('admin.conferences') }}"><i class="fa fa-btn fa-sign-out"></i>View all</a></li>--}}
                            {{--<li><a href="{{ route('admin.conferences.create') }}"><i class="fa fa-btn fa-sign-out"></i>Create</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                    {{--<li class="dropdown">--}}
                        {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">--}}
                            {{--User <span class="caret"></span>--}}
                        {{--</a>--}}
                        {{--<ul class="dropdown-menu" role="menu">--}}
                            {{--<li><a href="{{ route('admin.users') }}"><i class="fa fa-btn fa-sign-out"></i>View all</a></li>--}}
                            {{--<li><a href="{{ route('admin.users.create') }}"><i class="fa fa-btn fa-sign-out"></i>Create</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                {{--</ul>--}}

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (!Auth::guest())
                        <img class="pull-left vcc-top" src="{{ asset('public/img/vcc-logo.png') }}">
                        <a href="{{ url('/logout') }}" id="logout-bt"><img src="{{ asset('public/img/logout.png') }}"></a>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    <div class="container main-container" style="width:90%">
        @yield('content')
    </div>

    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="{{ asset('public/customscroll/jquery.mCustomScrollbar.js') }}"></script>
    @yield('js')

    <script>
        $(".chat-box").mCustomScrollbar();
    </script>


</div>
<div class="footer">
    <div class="container" style="width: 90%">
        <div class="col-md-3">
            <a href="#">Disclaimer</a>
            <a href="#"> Legal notice and Terms of Use</a>
        </div>
        <div class="col-md-5 text-center">
            brought to you by <br>
            <img src="{{ asset('public/img/lb-logo.png') }}" height="60"><br>
            @Leading Brands 2016
        </div>
    </div>
</div>
</body>
</html>
