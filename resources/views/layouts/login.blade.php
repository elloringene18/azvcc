<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ asset('public/customscroll/jquery.mCustomScrollbar.min.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/main.css') }}" rel="stylesheet">

    <style>
        body {
            background-image: url({{ asset('public/img/log-bg.png') }});
            background-size:  100% auto;
            overflow: hidden;
        }

        .fa-btn {
            margin-right: 6px;
        }
        ::-webkit-input-placeholder { /* WebKit, Blink, Edge */
            color:    #fff;
        }
        :-moz-placeholder { /* Mozilla Firefox 4 to 18 */
           color:    #fff;
           opacity:  1;
        }
        ::-moz-placeholder { /* Mozilla Firefox 19+ */
           color:    #fff;
           opacity:  1;
        }
        :-ms-input-placeholder { /* Internet Explorer 10-11 */
           color:    #fff;
        }
        :placeholder-shown { /* Standard (https://drafts.csswg.org/selectors-4/#placeholder) */
          color:    #fff;
        }
        .form-control {
            background: transparent;
            font-size: 28px;
            height: 70px;
            width: 100%;
            color: #fff;
            padding: 20px 15px;
            border: 2px solid #000;
            border-radius: 0;
            border-left: 0;
            margin: 0;
            border-right: 0;
        }
        .form-control:focus {
            border-color: #000;
            box-shadow:  none;
        }
        .form-horizontal .form-group {
            margin: 0;
        }
        a {
            color: #DCDCDC !important;
            font-size: 15px !important;
        }
        .login-bt {
            color: #ccc !important;
            font-size: 27px !important;
            background-color: #333333;
            border: 0;
            padding: 0 10px;
        }
        .login-bt:focus {
            border: 0;
             box-shadow:  none;
         }

         #az-logo {
            position: absolute;
            right: 0;
            top: 40%;
         }

         #lb-logo {
            position: absolute;
            right: 0;
            bottom: 3%;
         }
         #lb-logo span{
            color: #cecece;
            font-size: 18px;
         }
         #lb-logo img{
            margin-top: -7px;
         }
         a {
            text-decoration: none !important;
         }
    </style>
</head>
<body id="app-layout">
    <div class="container main-container" style="width:95%">
        @yield('content')
    </div>

    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="{{ asset('public/customscroll/jquery.mCustomScrollbar.js') }}"></script>
    @yield('js')


</div>
</body>
</html>
