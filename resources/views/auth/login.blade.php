@extends('layouts.login')

@section('content')
    <div class="row">
        <div class="full-height col-md-3 login-bg no-padding">
            <div class="vcc-logo text-center">
                <img src="{{ asset('public/img/vcc-logo-large.png') }}">
            </div>
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                {!! csrf_field() !!}

                @if (isset($_GET['room']))
                    <input type="hidden" name="room_id" value="{{ $_GET['room'] }}">
                @endif

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <div class="col-md-12 no-padding">
                        <input type="text" class="form-control border-b-0" name="email" value="{{ old('email') }}" placeholder="USERNAME">

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif

                    </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <div class="col-md-12 no-padding">
                        <input type="password" class="form-control" name="password" placeholder="PASSWORD">

                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                {{--<div class="form-group">--}}
                    {{--<div class="col-md-6 col-md-offset-4">--}}
                        {{--<div class="checkbox">--}}
                            {{--<label>--}}
                                {{--<input type="checkbox" name="remember"> Remember Me--}}
                            {{--</label>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}

                <div class="form-group" style="margin-top: 10px">
                    <div class="col-md-6 text-left">
                        <a class="btn btn-link no-padding" href="{{ url('/password/reset') }}">Forgot Password</a>
                    </div>
                    <div class="col-md-6 text-right">
                        <button type="submit" class="login-bt">
                            Login
                        </button>

                    </div>
                </div>
            </form>
        </div>

        <div class="full-height col-md-4 col-md-offset-2 relative">
            <img id="az-logo" src="{{ asset('public/img/az-logo-white.png') }}">
        </div>

        <div class="full-height col-md-3 relative">
           <div class="" id="lb-logo" >
             <span>brought to you by</span> <img src="{{ asset('public/img/lb-logo.png') }}">
           </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        var body = document.body,
            html = document.documentElement;

        var elems = document.getElementsByClassName('full-height');

        var height = Math.max( body.scrollHeight, body.offsetHeight,
               html.clientHeight, html.scrollHeight, html.offsetHeight );

        window.onresize = function(event) {
            setTimeout(function() {
                resizeHeight();
            },100);
        };

        resizeHeight();

        function resizeHeight(){

            elems = document.getElementsByClassName('full-height');

            for (var i = 0; i < elems.length; i++)
                elems[i].style.height = 0;

            body = document.body,
            html = document.documentElement;
            height = 0;
            console.log(body.scrollHeight);
            height = Math.max( body.scrollHeight, body.offsetHeight,
                   html.clientHeight, html.scrollHeight, html.offsetHeight );

            console.log('resizing to ' + height);
            for (var i = 0; i < elems.length; i++)
                elems[i].style.height = height+'px';
        }
    </script>
@endsection
