@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('public/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/owl.theme.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/owl.transitions.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/owl.carousel.css') }}">
@endsection

@section('content')
<div class="container" style="width:1240px">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Room</div>

                <div class="panel-body">

                          <div class="col-md-2 group">
                            <div class="row">
                              <div id="connectionsPanel">
                                <div class="panel panel-default">
                                  <div class="panel-heading">
                                    <h3 class="panel-title">Users online</h3>
                                  </div>
                                  <div class="panel-body">
                                    <div id="subscribers">
                                    </div>
                                    <button id="dcAll">Disconnect All</button>
                                  </div>
                                </div>
                                <div class="panel panel-default">
                                  <div class="panel-heading">
                                    <h3 class="panel-title">Chat</h3>
                                  </div>
                                  <div class="panel-body">
                                    <textarea id="box-msg" class="width-100" rows="3"></textarea><br>
                                    <button id="submit-msg" style="display:none;">Send Message</button>

                                    <ul id="chat-messages">

                                    </ul>
                                  </div>
                                </div>

                                </div>
                            </div>
                    <!--         <div class="row">
                              <a target="_blank" href="http://leadingbrands.me">
                                <img width="100%" style="margin-bottom:20px" src="<?php echo $_SERVER['REQUEST_SCHEME']  ?>://<?php echo $_SERVER['HTTP_HOST'] . '/kolchat'  ?>/img/gifs/Conferences.gif">
                              </a>
                            </div>  -->
                          </div>


                          <div class="col-md-10 group">

                              <div id="slider" data-slider-id="1" class="owl-carousel owl-theme">
                                <div class="item"><img height="250" src="{{ asset('public/img/slides/slide1.jpg') }}" alt="The Last of us"></div>
                                <div class="item"><img height="250" src="{{ asset('public/img/slides/slide2.jpg') }}" alt="GTA V"></div>
                                <div class="item"><img height="250" src="{{ asset('public/img/slides/slide3.jpg') }}" alt="Mirror Edge"></div>
                                <div class="item"><img height="250" src="{{ asset('public/img/slides/slide4.jpg') }}" alt="The Last of us"></div>
                                <div class="item"><img height="250" src="{{ asset('public/img/slides/slide5.jpg') }}" alt="GTA V"></div>
                                <div class="item"><img height="250" src="{{ asset('public/img/slides/slide6.jpg') }}" alt="Mirror Edge"></div>
                                <div class="item"><img height="250" src="{{ asset('public/img/slides/slide7.jpg') }}" alt="The Last of us"></div>
                                <div class="item"><img height="250" src="{{ asset('public/img/slides/slide8.jpg') }}" alt="GTA V"></div>
                                <div class="item"><img height="250" src="{{ asset('public/img/slides/slide9.jpg') }}" alt="Mirror Edge"></div>
                              </div>

                              <div class="owl-thumbsy" data-slider-id="1">
                              </div>

                              <div class="panel panel-default">
                                <div class="panel-heading">
                                  <h3 class="panel-title">SESSIN</h3>
                                </div>
                                <div class="panel-body" id="publisherPanel">
                                  <div id="publisher" class="publisher">
                                  </div>
                                  <div id="user" class="user">

                                  </div>

                                  <input type="hidden" id="username" value="SESSIN">
                                </div>
                              </div>


                          </div>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')

    <script type="text/javascript">
        var apiKey = '<?php echo $apiKey; ?>';
        var sessionId = '<?php echo $sessionId; ?>';
        var token = '<?php echo $token; ?>';
        var name = 'SESSIN';
    </script>

    <script src="//static.opentok.com/webrtc/v2.2/js/TB.min.js"></script>

    <script src="{{ asset('public/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('public/js/OwlCarousel2Thumbs.min.js') }}"></script>
    <script src="{{ asset('public/opentok/room.js') }}"></script>


@endsection